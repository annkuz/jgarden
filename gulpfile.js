'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const debug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const imageMin = require('gulp-imagemin');
const htmlmin = require('gulp-htmlmin');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create();

const isDevelopment = false;

gulp.task('copyHtml', function(){
     return gulp.src('src/*.html', {since: gulp.lastRun('copyHtml')})
        .pipe(gulp.dest('public'));
});

// Gulp task to minify HTML files
gulp.task('htmlMin', function() {
    return gulp.src(['src/*.html'], {since: gulp.lastRun('htmlMin')})
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('public'));
});

gulp.task('copyPHP', function(){
     return gulp.src('src/*.php', {since: gulp.lastRun('copyPHP')})
        .pipe(gulp.dest('public'));
});

gulp.task('imageMin', function(){
     return gulp.src('src/images/*.jpg')
        .pipe(imageMin())
        .pipe(gulp.dest('public/images'));
});

gulp.task('minifyJs', function(){
     return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('styles', function(){
    return gulp.src('src/sass/**/*.sass')
       .pipe(gulpIf(isDevelopment, sourcemaps.init()))
       .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
       .pipe(gulpIf(isDevelopment, sourcemaps.write()))
       .pipe(gulp.dest('public/css'));
});


gulp.task('serve', function(done){
    browserSync.init({
        watch: true,
        server: 'public'
    });

    //browserSync.watch('public/*').on('change', browserSync.reload);
    done();
});

gulp.task('buildDev', gulp.series('styles', 'minifyJs', 'copyHtml', 'copyPHP'));
gulp.task('build', gulp.series('styles','imageMin', 'minifyJs', 'htmlMin', 'copyPHP'));

gulp.task('watch', function(){
    gulp.watch('src/sass/**/*.sass', gulp.series('styles'));
    gulp.watch('src/images/*', gulp.series('imageMin'));
    gulp.watch('src/js/*.js', gulp.series('minifyJs'));
    gulp.watch('src/*.html', gulp.series('copyHtml'));
});

gulp.task('dev', gulp.series('buildDev', gulp.parallel('watch', 'serve')));