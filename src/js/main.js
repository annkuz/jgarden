$(document).ready(function(){
    var menu_trigger = $('.menu-trigger,.close-trigger');
    var menu = $('.menu');
    var nav_link = $('.nav-h, .nav-item>a:first-child');

   var toggleMenu = function(){menu.toggleClass('menu_active');};

   menu_trigger.click(function(e){
        e.preventDefault();
        toggleMenu();
    });

   nav_link.click(function(e){
       e.preventDefault();
       toggleMenu();
   });


    // Smooth Scrolling
   $('a[data-target^="anchor"]').on('click', function(event) {
        if (this.hash !== '') {
            event.preventDefault();

            var hash = this.hash;

            $('html, body').animate(
                {
                    scrollTop: $(hash).offset().top - 130
                },
                1000
            );
        }
        return false;
    });

   $("#callback_form").on('click',function (e) {
        if(e.target.classList.contains('submit-btn')) {
            var input = $("#callback_form input[type='text']");

            $.ajax({
                type: "POST",
                url: "mail.php",
                data: $(this).serialize(),
                success: function (msg) {
                    if (msg == 'ok') {
                        input.removeClass("input-error");
                        input.val("");
                        $('#callback_form .form-message').html("<span class=\'form-success\' >Благодарим за заявку!</span>");
                    } else {
                        input.addClass("input-error");
                        $('#callback_form .form-message').html("<span class=\'form-error\' >" + msg + "</span>");
                    }
                }
            });
        }
       return false;
   });

    $(window).scroll(function(e){
        parallaxScroll();
    });

    function parallaxScroll(){
        var scrolled = $(window).scrollTop();
        $('.speed-1').css('top',(0-(scrolled*.25))+'px');
        $('.speed-2').css('top',(0-(scrolled*.4))+'px');
        $('.speed-3').css('top',(0-(scrolled*.75))+'px');
    }


    $(window).scroll(function() {
        var distanceTop = $('section.description-section').offset().top;
        if  ($(window).scrollTop() > distanceTop)
            $('.topbar .logo').animate({'opacity':'1'},200);
        else
            $('.topbar .logo').stop(true).animate({'opacity':'0'},200);
    });



});


/*  Для Управления Ютуб видео  */
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var playerElem = document.getElementById('player');
function onYouTubeIframeAPIReady() {
    console.log("iframe api ready");
    player = new YT.Player('player',{
        events: {
            'onReady': function(){console.log('ready!!')}
        }
    });
}

/*  end Для Ютуб видео  */

var popupCallback = document.getElementById('modal_callback');
var popupCallbackToggle = document.querySelector('.btn-callback');
var popupCallbackCloseBtn = document.querySelector('#modal_callback .close-popup-trigger');
var popupVideo = document.getElementById('modal_video');
var popupVideoToggle = document.querySelector('.header-bottom_video a');
var popupVideoCloseBtn = document.querySelector('#modal_video .close-popup-trigger');

popupCallbackToggle.onclick = function(e) {
    e.preventDefault();
    popupCallback.style.display = "flex";
    return false
};

popupCallbackCloseBtn.onclick = function (e) {
    e.preventDefault();
    popupCallback.style.display = "none";
    return false
};
popupVideoToggle.onclick = function(e) {
    e.preventDefault();
    popupVideo.style.display = "flex";
    player.playVideo();
    return false
};

popupVideoCloseBtn.onclick = function (e) {
    e.preventDefault();
    popupVideo.style.display = "none";
    player.pauseVideo();
    return false
};

document.querySelector('body').onclick = function (e) {
     // e.preventDefault();
    if( e.target == popupCallback || e.target == popupVideo) {
       e.preventDefault();
        e.target.style.display = "none";
        if(e.target == popupVideo) {
            player.pauseVideo();
        }
         return false;
    }
    // return false;
};



document.querySelector('body').addEventListener('click', function(e){
    var elem = e.target;
    if(elem.classList.contains("croped-text")){
        elem.querySelector('.ellipsis').style.display="none";
        //elem.querySelector('.hidden-text-part').style.display="inline";
       //elem.querySelector('.hidden-text-part').classList.add('opened-text');
        elem.querySelector('.hidden-text-part').style.maxHeight='1000px';
        elem.querySelector('.hidden-text-part').style.display='inline';
        var sibling = elem.nextElementSibling;
        console.log(sibling);
        if( sibling.classList.contains('croped-extra-block')){
            sibling.style.display='block!important';
        }
        elem.classList.remove('croped-text');
    }
});



var isIOS = /iPad|iPhone|iPod/.test(navigator.platform);

if (isIOS) {

    var canvasVideo = new CanvasVideoPlayer({
        videoSelector: '.video',
        canvasSelector: '.canvas',
        timelineSelector: false,
        autoplay: true,
        makeLoop: true,
        pauseOnClick: false,
        audio: false
    });

}else {

    // Use HTML5 video
    document.querySelectorAll('.canvas')[0].style.display = 'none';

}


