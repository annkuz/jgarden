var ctrl = new ScrollMagic.Controller({
  globalSceneOptions: {
    triggerHook: 'onLeave'
  }
});

$('.parallaxscroll').each(function() {
	new ScrollMagic.Scene({
		triggerElement: this
	})
	.setPin(this)
	.addTo(ctrl);
});
