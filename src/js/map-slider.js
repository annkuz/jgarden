$(document).ready(function() {
    $('.sl').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        arrows: true,
        asNavFor: '.sl2',
        dots: true,
        vertical:true,
        infinite: false,
        verticalSwiping: true,
        draggable: true,
        nextArrow: document.querySelector('.slick-next'),
        prevArrow: document.querySelector('.slick-prev'),
        dotsClass: 'slick-dots first-slick'
    });
    $('.sl2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        draggable: true,
        vertical:true,
        verticalSwiping: true,
        infinite: false,
        asNavFor: '.sl',
        arrows: false,
        dots: true,
        dotsClass: 'slick-dots second-slick'
    });

    function mapSlideTurnActive(){
        if($('ul.second-slick > li').first().hasClass('slick-active')) {
            $('.first-slick').css({'display': 'block'});
            $('.slick-next').css({'display': 'block'});
            $('.sl2').css({'display': 'none'});
            $('.first-slick li').addClass('animated slideInDown');
            $('.slider-back-btn').removeClass('white-slider-back-btn');
        }
    }

    function mapSlideTurnInactive(){
        if($('ul.second-slick > li').first().hasClass('slick-active')) {
            $('.first-slick').css({'display': 'none'});
            $('.slick-next').css({'display': 'none'});
            $('.sl2').css({'display': 'block'});
            $('.first-slick li').removeClass('animated slideInDown');
            $('.slider-back-btn').addClass('white-slider-back-btn');
        }
    }

    function openMapSlider(){
        $('.first-slick li').removeClass('animated slideInDown');
        $('.map-slider').addClass('active-map-slider');
        $('.topbar .logo a').first().css({'display': 'none'});
        $('.topbar .logo .logo-for-map-slider').css({'display': 'inline-block'});

        setTimeout(function(){
                $('.first-slick li').addClass('animated slideInDown');
                $('.slider-back-btn').css({'display': 'inline-block'});
                $('.slider-back-btn').animate({'opacity':'1'},200);
                $('.slider-back-btn').addClass('animated slideInDown');
                $('.topbar .logo').animate({'opacity':'1'},200);
            }, 1000);

    }

    function closeMapSlider(){
        $('.map-slider').removeClass('active-map-slider');
        $('.topbar .logo').animate({'opacity':'0'},200);
        setTimeout(function(){
                $('.topbar .logo a').first().css({'display': 'inline-block'});
                $('.topbar .logo .logo-for-map-slider').css({'display': 'none'});
            }, 200);

        $('.slider-back-btn').css('opacity','0');
        $('.slider-back-btn').css({'display': 'none'});
        $('.slider-back-btn').removeClass('animated slideInDown');
        setTimeout(function() {
            $('.sl2').slick('slickGoTo', 0, false);
        },1000);
    }

    var inscroll = false; //происходит ли сейчас действие в слайдере по скроллу
    var mapWidth = $('.sl .slide-1 .map-center-img').width();
    //var newWrapper = $('<div/>').addClass('first-slick-wrap');

    //$('.sl .first-slick').wrapAll(newWrapper);
   // $('div.first-slick-wrap').width(mapWidth);
    $('.sl .first-slick').width(mapWidth);
    $('.sl .first-slick').css('left',(($(window).width()-mapWidth)/2)+'px');
   // $('div.first-slick-wrap').css('left',(($(window).width()-mapWidth)/2)+'px');


//click-dots on map
    $('ul.first-slick > li').click(function () {
        mapSlideTurnInactive();
    });

//click-button start walk
    $('.slick-next').click(function () {
        mapSlideTurnInactive();
    });

    $('.sl').on('wheel', (function(e) {
        e.preventDefault();

        if(inscroll) return false;

        if(!inscroll) {
            inscroll = true;
            if (e.originalEvent.deltaY > 0) {
                if ($(this).slick('slickCurrentSlide') == $(this).find('.slick-slide').length - 1) {
                    closeMapSlider();
                } else {
                    $(this).slick('slickNext');
                }
            } else {
                $(this).slick('slickPrev');
            }
        }
    }));


    $('.map-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        mapSlideTurnActive();
        $('.sl .slick-current .map-h').addClass('animated fadeInDown');
        $('.sl .slick-current .map-h .h-line-2').addClass('animated fadeInUp');
        inscroll = false;
    });

    $('.map-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.sl .slick-current .map-h').removeClass('animated fadeInDown');
        $('.sl .slick-current .map-h .h-line-2').removeClass('animated fadeInUp');
        mapSlideTurnInactive();

    });

    $('.header-bottom_walk a').click(function(e){
        if (this.hash !== '') {
            e.preventDefault();

            var hash = this.hash;

            $('html, body').animate(
                {
                    scrollTop: $(hash).offset().top
                },
                1000
            );
        }
        openMapSlider();
        return false;
    });
    $('.slider-back-btn').click(function(e){
        closeMapSlider();
    });

});