$(document).ready(function(){
    var scrollable = $('.scrollable');
    var scrollable_img = $('.img-back');
    var scrollable_img_front = $('.img-front');
    var scrollable_words_odd = $('.section-top-img .layer-words>span:nth-child(odd) span');
    var scrollable_words_even = $('.section-top-img .layer-words>span:nth-child(even) span');
    var scrollable_top = [];
    var scrollable_top_img = [];
    var scrollable_top_img_front = [];
    var scrollable_top_words_odd = [];
    var scrollable_top_words_even = [];
    var lastScrollTop = 0;
    var scroll_direction;

    function getOffsetTop(scrollable_elems, top_arr){
        for(var i=0; i<scrollable_elems.length; i++){
            top_arr[i] = $(scrollable_elems[i]).offset().top;
        }
    }

    getOffsetTop(scrollable, scrollable_top);
    getOffsetTop(scrollable_img, scrollable_top_img);
    getOffsetTop(scrollable_img_front, scrollable_top_img_front);
    getOffsetTop(scrollable_words_odd, scrollable_top_words_odd);
    getOffsetTop(scrollable_words_even, scrollable_top_words_even);

    // for(var i=0; i<scrollable.length; i++){
    //     scrollable_top[i] = $(scrollable[i]).offset().top;
    // }
    // for(var i=0; i<scrollable_img.length; i++){
    //     scrollable_top_img[i] = $(scrollable_img[i]).offset().top;
    // }
    // for(var i=0; i<scrollable_img_front.length; i++){
    //     scrollable_top_img_front[i] = $(scrollable_img_front[i]).offset().top;
    // }


    function getTranslateYValue(elem){
        var matrix = $(elem).css('transform').replace(/[^0-9\-.,]/g, '').split(',');
        y = parseFloat(matrix[13]) || parseFloat(matrix[5]);
        console.log(y);
        return y? y: 0;
    };


    //  function checkPosition(item, top_scroll, scroll_direction){
    //     scroll_direction == 1 ? $(item).css({"transform": "translateY("+(getTranslateYValue(item)-2)+"px)"}) : $(item).css({"transform": "translateY("+(getTranslateYValue(item)+2)+"px)"});
    //
    // }
     function checkPosition(item, step, scroll_direction){
         var topVal = parseFloat($(item).css("top"));
        scroll_direction == 1 ? $(item).css({"top": topVal-step}) : $(item).css({"top": topVal+step});
     }

    $(window).scroll(function(){
        var top_scroll = $(document).scrollTop();
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            scroll_direction = 1;
        } else {
            scroll_direction = 0;
        }
        lastScrollTop = st;
        var winHeight = $(window).height();

        top_scroll = top_scroll+ winHeight;
        scrollable_top.forEach(function(el, key){
            var elemHeight = $(scrollable[key]).height();
            if(top_scroll > el && (top_scroll<=el+winHeight+elemHeight)) {
                //console.log('top_scroll = '+top_scroll);
                // console.log('el = '+el);
                 //console.log('scrollable'+key+' css top (top > el) = '+$(scrollable[key]).css("top"));
                checkPosition(scrollable[key], 2, scroll_direction);

            }
            //else if (top_scroll < el || (top_scroll>el+winHeight+elemHeight)){
               // $(scrollable[key]).css({"top":""});
                // console.log('scrollable'+key+' css top (top < el)= '+$(scrollable[key]).css("top"));
            //}
        });
        scrollable_top_img.forEach(function(el, key){

            var elemHeight = $(scrollable_img[key]).height();

            if(top_scroll > el && (top_scroll<=el+winHeight+elemHeight)) {
                checkPosition(scrollable_img[key], 3, scroll_direction);
            }
        });
        /*scrollable_top_img_front.forEach(function(el, key){

            var elemHeight = $(scrollable_img_front[key]).height();

            if(top_scroll > el && (top_scroll<=el+winHeight+elemHeight)) {
                checkPosition(scrollable_img_front[key], 0.7, (scroll_direction == 1? 0:1));
            }
        });*/
        scrollable_top_words_even.forEach(function(el, key){

            var elemHeight = $(scrollable_words_even[key]).height();

            if(top_scroll > el && (top_scroll<=el+winHeight+elemHeight)) {
                checkPosition(scrollable_words_even[key], 1, (scroll_direction == 1? 0:1));
            }
        });
        scrollable_top_words_odd.forEach(function(el, key){

            var elemHeight = $(scrollable_words_odd[key]).height();

            if(top_scroll > el && (top_scroll<=el+winHeight+elemHeight)) {
                checkPosition(scrollable_words_odd[key], 1, scroll_direction);
            }
        });
    });
});